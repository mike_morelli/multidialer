#include "MDActionSheetDelegate.h"

extern void (*$$_CTCallDialWithID)(NSString* number,int abUID);
extern BOOL $_passthru;

@implementation MDActionSheetDelegate
-(id)initWithOptions:(NSArray*)_options {
  if((self=[super init])){options=[_options retain];}
  return self;
}
-(void)actionSheet:(UIActionSheet*)sheet clickedButtonAtIndex:(NSInteger)index {
  if(index<sheet.cancelButtonIndex){
    id URL=[options objectAtIndex:index];
    if([URL isKindOfClass:[NSString class]]){
      $_passthru=YES;
      $$_CTCallDialWithID(URL,-1);
      $_passthru=NO;
    }
    else {[[UIApplication sharedApplication] openURL:URL];}
  }
  [self release];
}
-(void)dealloc {
  [options release];
  [super dealloc];
}
@end
