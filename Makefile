include theos/makefiles/common.mk

TWEAK_NAME = MultiDialer GVURLScheme

MultiDialer_FILES = Tweak.x MDActionSheetDelegate.m
MultiDialer_FRAMEWORKS = UIKit
MultiDialer_PRIVATE_FRAMEWORKS = SpringBoardServices

GVURLScheme_FILES = gvhelper.x

include $(THEOS_MAKE_PATH)/tweak.mk

USERPREFS_DIR = $(THEOS_STAGING_DIR)/Library/Preferences

internal-tweak-stage:: $(USERPREFS_DIR)
$(USERPREFS_DIR) : defaults.plist ; mkdir -vp $@ && ln $< $@/com.officialscheduler.multidialer.plist
